CXX=g++
CXXFLAGS=-Wall -Wextra -Werror -pedantic -std=c++11

SRC_DIR=src/

SRC=${wildcard $(SRC_DIR)*.cc}

OBJ=${SRC:.cc=.o}

TARGET=plam

all:
	$(CXX) $(CXXFLAGS) $(SRC) -lglpk -lm -o $(TARGET)

clean:
	$(RM) $(TARGET)
