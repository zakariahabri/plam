#include "problem.h"

#include <glpk.h>
#include <queue>

Problem::Problem()
    : nb_possible_factories(0)
    , nb_needed_factories(0)
    , factory_distances(nullptr)
    , factories_chosen(nullptr)
    , max_cumulated_space(0)
{
}

Problem::~Problem()
{
    if (factory_distances != nullptr)
    {
        for (unsigned int i = 0; i < nb_possible_factories; i++)
            if (!factory_distances[i])
                delete factory_distances[i];
    }
    delete factory_distances;

    if (factories_chosen != nullptr)
        delete[] factories_chosen;
}

int Problem::read_file(std::istream& par_stream)
{
    // reading the number of candidate factories
    par_stream >> nb_possible_factories;

    // checking if the number of candidate factories is correct
    if (nb_possible_factories == 0 || nb_possible_factories > 1000)
        return 1;

        factories.resize(nb_possible_factories);
        // allocating the upper triangle array for factory distances storage
        factory_distances = new int*[nb_possible_factories - 1];
        for (unsigned int i = 0; i < nb_possible_factories - 1; i++)
            factory_distances[i] = new int[nb_possible_factories - 1 - i];

    if (par_stream.get() != ' ')
        return 1;

    // reading the number of needed factories
    par_stream >> nb_needed_factories;

    // checking if there is enough factories availables
    if (nb_needed_factories == 0 || nb_needed_factories > nb_possible_factories)
        return 1;

    unsigned int index;
    for (unsigned int i = 0; i < nb_possible_factories; i++)
    {
        // reading the Windows EOL
        if (par_stream.get() != '\r' && par_stream.get() != '\n')
            return 1;

        // reading the factory index
        par_stream >> index;
        if (index == 0
            || index > nb_possible_factories
            || par_stream.get() != ' ')
            return 1;
        index--;

        // reading the x-coordinate of the current factory
        par_stream >> factories[index].x;
        if (par_stream.get() != ' ')
            return 1;

        // reading the y-coordinate of the current factory
        par_stream >> factories[index].y;
        if (par_stream.get() != ' ')
            return 1;

        // reading the space of the current factory
        par_stream >> factories[index].space;
    }

    return 0;
}

void Problem::calculate_distances()
{
    int x, y;
    for (unsigned int i = 0; i < nb_possible_factories - 1; i++)
    {
        for (unsigned int j = i + 1; j < nb_possible_factories; j++)
        {
            x = factories[i].x - factories[j].x;
            y = factories[i].y - factories[j].y;
            factory_distances[i][j-i-1] = x * x + y * y;
        }
    }
}

void Problem::solve1()
{
    int is[1000+1];
    int js[1000+1];
    double cs[1000+1];
    glp_prob* lp = nullptr;

    lp = glp_create_prob();
    glp_set_obj_dir(lp, GLP_MAX);

    glp_add_cols(lp, nb_possible_factories);
    for (unsigned int i = 0; i < nb_possible_factories; i++)
    {
        glp_set_obj_coef(lp, i+1, factories[i].space);
        glp_set_col_kind(lp, i+1, GLP_BV);
    }

    std::vector<std::pair<unsigned int, unsigned int>> absurd_couples;
    get_absurd_couples(absurd_couples);

    glp_add_rows(lp, absurd_couples.size()+1);

    for (unsigned int i = 0; i < absurd_couples.size(); i++)
    {
        glp_set_row_bnds(lp, i+1, GLP_UP, 0.0, 1.0);

        is[2*i+1] = i + 1;
        js[2*i+1] = absurd_couples[i].first + 1;
        cs[2*i+1] = 1.0;

        is[2*i+2] = i + 1;
        js[2*i+2] = absurd_couples[i].second + 1;
        cs[2*i+2] = 1.0;
    }

    glp_set_row_bnds(lp, absurd_couples.size()+1, GLP_UP, 0.0, nb_needed_factories);
    for (unsigned int i = 0; i < nb_possible_factories; i++)
    {
        is[2*absurd_couples.size()+1+i] = absurd_couples.size() + 1;
        js[2*absurd_couples.size()+1+i] = i + 1;
        cs[2*absurd_couples.size()+1+i] = 1.0;
    }

    glp_load_matrix(lp, 2*absurd_couples.size()+nb_possible_factories, is, js, cs);
    glp_simplex(lp, nullptr);
    glp_intopt(lp, nullptr);

    max_cumulated_space = glp_mip_obj_val(lp);
    factories_chosen = new bool[nb_possible_factories];
    for (unsigned int i = 0; i < nb_possible_factories; i++)
        factories_chosen[i] = (glp_mip_col_val(lp, i+1) == 1.0);

    glp_delete_prob(lp);
}

void Problem::solve2()
{
    int is[1000+1];
    int js[1000+1];
    double cs[1000+1];
    glp_prob* lp = nullptr;

    lp = glp_create_prob();
    glp_set_obj_dir(lp, GLP_MAX);

    std::vector<std::vector<unsigned int>> possible_sets;
    get_possible_sets(possible_sets);
    unsigned int possible_sets_size = possible_sets.size();

    glp_add_cols(lp, possible_sets_size);
    glp_add_rows(lp, 2);

    glp_set_row_bnds(lp, 1, GLP_UP, 0.0, 1.0);
    glp_set_row_bnds(lp, 2, GLP_UP, 0.0, 0.0);

    for (unsigned int i = 0; i < possible_sets_size; i++)
    {
        glp_set_col_kind(lp, i+1, GLP_BV);
        double space = get_space_possible_set(possible_sets[i]);
        glp_set_obj_coef(lp, i+1, space);
        is[i+1] = (space < 0) ? 2 : 1;
        js[i+1] = i + 1;
        cs[i+1] = 1.0;
    }

    glp_load_matrix(lp, possible_sets_size, is, js, cs);
    glp_simplex(lp, nullptr);
    glp_intopt(lp, nullptr);

    max_cumulated_space = glp_mip_obj_val(lp);
    factories_chosen = new bool[nb_possible_factories];
    unsigned int sol_set_index = 0;
    for (unsigned int i = 0; i < possible_sets_size; i++)
    {
        if (glp_mip_col_val(lp, i+1) == 1.0)
        {
            sol_set_index = i;
            break;
        }
    }

    factories_chosen = new bool[nb_possible_factories] { false };
    std::vector<unsigned int>& sol_set = possible_sets[sol_set_index];
    for (unsigned int k = 0; k < sol_set.size(); k++)
        factories_chosen[sol_set[k]] = true;

    glp_delete_prob(lp);
}

void Problem::print_result(std::ostream& par_stream)
{
    if (factories_chosen == nullptr)
        return;

    for (unsigned int i = 0; i < nb_possible_factories; i++)
    {
        if (factories_chosen[i])
            par_stream << i+1 << ' ';
    }
}

void Problem::print_factories(std::ostream& par_stream)
{
    for (unsigned int i = 0; i < nb_possible_factories; i++)
        par_stream << '(' << factories[i].x << ','
            << factories[i].y << ','
            << factories[i].space << ')'
            << std::endl;
}

void Problem::print_factory_distances(std::ostream& par_stream)
{
    par_stream << '[';
    for (unsigned int i = 0; i < nb_possible_factories - 1; i++)
    {
        par_stream << '[';
        for (unsigned int j = 0; j < nb_possible_factories - i - 1; j++)
            par_stream << factory_distances[i][j] << ' ';
        par_stream << "] ";
    }
    par_stream << ']' << std::endl;
}

void Problem::get_absurd_couples(std::vector<std::pair<unsigned int, unsigned int>>& par_couples)
{
    if (factory_distances == nullptr)
        return;

    for (unsigned int i = 0; i < nb_possible_factories - 1; i++)
        for (unsigned int j = i + 1; j < nb_possible_factories; j++)
            if (get_distance(i, j) > 2500)
                par_couples.emplace_back(std::pair<unsigned int, unsigned int>(i, j));
}

void Problem::get_possible_sets(std::vector<std::vector<unsigned int>>& par_sets)
{
    if (factory_distances == nullptr)
        return;

    std::queue<std::vector<unsigned int>> tmp_queue;
    
    // getting possible first indexes
    for (unsigned int i = 0; i < nb_possible_factories - nb_needed_factories + 1; i++)
    {
        std::vector<unsigned int> tmp_vect;
        tmp_vect.push_back(i);
        tmp_queue.emplace(tmp_vect);
    }

    // getting all possible sets
    for (unsigned int k = 1; k < nb_needed_factories; k++)
    {
        unsigned int tmp_size = tmp_queue.size();
        for (unsigned int n = 0; n < tmp_size; n++)
        {
            std::vector<unsigned int> tmp_vect = tmp_queue.front();
            for (unsigned int i = tmp_vect.back() + 1; i < nb_possible_factories - nb_needed_factories + k + 1; i++)
            {
                std::vector<unsigned int> new_vect(tmp_vect);
                new_vect.push_back(i);
                tmp_queue.emplace(new_vect);
            }
            tmp_queue.pop();
        }
    }

    // putting the possible sets in the vector
    while (!tmp_queue.empty())
    {
        par_sets.emplace_back(tmp_queue.front());
        tmp_queue.pop();
    }

    //TEMP
    // for (unsigned int i = 0; i < par_sets.size(); i++)
    // {
    //     std::cout << '[';
    //     for (unsigned int j = 0; j < par_sets[i].size(); j++)
    //         std::cout << par_sets[i][j] << ',';
    //     std::cout << ']' << std::endl;
    // }
}

double Problem::get_space_possible_set(std::vector<unsigned int>& par_set)
{
    unsigned int tmp_size = par_set.size();
    for (unsigned int i = 0; i < tmp_size - 1; i++)
        for (unsigned int j = i + 1; j < tmp_size; j++)
            if (get_distance(par_set[i], par_set[j]) > 2500)
                return -1;

    double res = 0;
    for (unsigned int i = 0; i < tmp_size; i++)
        res += factories[i].space;

    return res;
}

std::vector<std::vector<int>> Problem::get_correct_groups()
{
    std::vector<std::vector<int>> lv2;
    std::vector<std::vector<int>> lv3;
    std::vector<std::vector<int>> lv4;
    for (unsigned int i = 0; i < nb_possible_factories; i++)
    {
        for (unsigned int j = 0; j < nb_possible_factories; j++)
        {
            if (i != j && get_distance(i,j) < 2500)
            {
                std::vector<int> tmp;
                tmp.push_back(i);
                tmp.push_back(j);
                lv2.push_back(tmp);
            }
        }
    }
    for (unsigned int i = 0; i < lv2.size(); i++)
    {
        for (unsigned int j = 0; j < nb_possible_factories; j++)
        {
            if (std::find(lv2[i].begin(), lv2[i].end(), j) == lv2[i].end()
             && get_distance(lv2[i][1], j) < 2500)
            {
                std::vector<int> tmp;
                tmp.push_back(lv2[i][0]);
                tmp.push_back(lv2[i][1]);
                tmp.push_back(j);
                lv3.push_back(tmp);
            }
        }
    }

    for (unsigned int i = 0; i < lv3.size(); i++)
    {
        for (unsigned int j = 0; j < nb_possible_factories; j++)
        {
            if (std::find(lv3[i].begin(), lv3[i].end(), j) == lv3[i].end()
             && get_distance(lv3[i][2], j) < 2500)
            {
                std::vector<int> tmp;
                tmp.push_back(lv3[i][0]);
                tmp.push_back(lv2[i][1]);
                tmp.push_back(lv2[i][2]);
                tmp.push_back(j);
                lv4.push_back(tmp);
            }
        }
    }

    return lv4;
}

int Problem::get_distance(unsigned int par_first_index, unsigned int par_other_index)
{
    if (factory_distances == nullptr
        || par_first_index >= nb_possible_factories
        || par_other_index >= nb_possible_factories)
        return -1;

    if (par_first_index == par_other_index)
        return 0;

    if (par_first_index < par_other_index)
        return factory_distances[par_first_index][par_other_index - par_first_index - 1];

    return factory_distances[par_other_index][par_first_index - par_other_index - 1];
}
