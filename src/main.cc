#include <fstream>
#include "problem.h"

int main()
{
	std::ifstream file("Choix_sites.txt");

	if (!file.is_open())
		return 1;

	Problem prob;
	if (prob.read_file(file) != 0)
		return 1;

	prob.calculate_distances();
	prob.solve1();
	prob.print_result(std::cout);

	file.close();
    return 0;
}
