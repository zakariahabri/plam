#ifndef PROBLEM_H
#define PROBLEM_H

#include <algorithm>
#include <iostream>
#include <utility>
#include <vector>

struct Factory
{
    int x;
    int y;
    double space;
};

class Problem
{
public:
    Problem();
    ~Problem();

    int read_file(std::istream& par_stream);
    void calculate_distances();
    void solve1();
    void solve2();
    void print_result(std::ostream& par_stream);
    void print_factory_distances(std::ostream& par_stream);
    void print_factories(std::ostream& par_stream);

private:
    void get_absurd_couples(std::vector<std::pair<unsigned int, unsigned int>>& par_couples);
    void get_possible_sets(std::vector<std::vector<unsigned int>>& par_sets);
    double get_space_possible_set(std::vector<unsigned int>& par_set);
    std::vector<std::vector<int>> get_correct_groups();
    int get_distance(unsigned int par_first_index, unsigned int par_other_index);

    unsigned int nb_possible_factories;
    unsigned int nb_needed_factories;
    int** factory_distances;
    bool* factories_chosen;
    double max_cumulated_space;
    std::vector<Factory> factories;
};

#endif // !PROBLEM_H
